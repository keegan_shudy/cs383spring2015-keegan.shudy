package Follow;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
//import lejos.robotics.RangeFinderAdapter;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.motor.Motor;
import lejos.robotics.navigation.DifferentialPilot;
//import lejos.robotics.navigation.Move;
//import lejos.robotics.objectdetection.RangeFeatureDetector;

public class Follower{ 
	private static final Port portU = LocalEV3.get().getPort(portMenu("Ultra"));
	private static final EV3UltrasonicSensor sonic = new EV3UltrasonicSensor(portU);
	private static final DifferentialPilot pilot = new DifferentialPilot(4.0f,14.0f,Motor.B, Motor.C);//4.0,14.0
	private static final SampleProvider distance= sonic.getMode("Distance");
	private static float[] sample = new float[distance.sampleSize() + 2];
	
	private static int timesLookedLeft = 0;
	private static int timesLookedRight = 0;
	

	
	private static final double MAX_LOST_DISTANCE = .8; //how far the object has to be before it looks for it

	public static void main(String [] robot) throws InterruptedException{
		
		
		distance.fetchSample(sample, 0);
		for(distance.fetchSample(sample,0); (sample[0] > .1) &&(Button.ESCAPE.isUp());distance.fetchSample(sample,0)){
			
			pilot.forward();
			//possibly: pilot.travel(sample[0], true);
			LCD.drawString("Distance: " + sample[0], 0,1);
			//if we've lost the object from our sights, find it
			if(sample[0] > MAX_LOST_DISTANCE /*|| /*((sample[0] <= sample[2]) && sample[0] <= sample[1])*/){
				distance.fetchSample(sample,1);
				//pilot.stop();
				if(lookForObject()){ pilot.forward(); distance.fetchSample(sample,2);} //might add more logic here
				else{
					turnAround();
				}
			}
		}
		pilot.stop();
		sonic.close();
		LCD.clear();
		LCD.drawString("Got it",1,1);
		Button.waitForAnyPress();
	
	}
	
	public static boolean lookForObject(){
		for(distance.fetchSample(sample,0); sample[0] > MAX_LOST_DISTANCE;distance.fetchSample(sample,0)){
			if(timesLookedLeft <= 2 && lookToFind(1,30)){timesLookedLeft=0; return true;}
			else if(timesLookedRight <= 3 && lookToFind(-1,90)){timesLookedRight=0; return true;}
		}
		return false;
	}
	
	/**
	 * 
	 * Tries to survey the robots surroundings to see if it can find the lost object
	 * @param direction - (1) to look left (-1) to look right
	 * @param distanceT - the arc distance that the robot will look
	 * @return true if the robot finds the object
	 * 			false otherwise
	 */
	
	private static boolean lookToFind(int direction, double distanceT){
		//might need to use steer(40,(-)distanceT,true) instead
		pilot.setRotateSpeed(180);
		if(direction == 1){ pilot.steer(40, distanceT, true); timesLookedLeft++;}
		else if(direction == -1){ pilot.steer(-40, -distanceT, true); timesLookedRight++;}
		//Move move = pilot.getMovement();
		//pilot.steer(40,90,true);
		do{
			distance.fetchSample(sample, 0);
			LCD.drawString("Distance:" + sample[0], 0,5);
			if(sample[0] < MAX_LOST_DISTANCE) return true;
		}while(pilot.isMoving());
		return false;
	}
	
	
	public static void turnAround(){
		pilot.rotate(185);
	
	}
	/**
	 * displays a text menu for the user to select a port for a sensor
	 * @param Type - the name of the sensor that is being assigned to a port
	 * @return - a port value as a string ... ie S1, S2...
	 */
	
	public static String portMenu(String Type){
		String ports[] = {"Port 1", "Port 2", "Port 3", "Port 4"};
        	TextMenu portMenu = new TextMenu(ports, 1, "Select " + Type + " Port");
        	int number = portMenu.select() + 1;
        	LCD.clear();
        	return "S" + number;
    }
}
import java.io.IOException;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.Sound;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;


public class Ultrasonic {
		public static void main(String[] args) throws IOException {
			float frequency = 1; // 1 sample per second
			System.out.println("Any button to start");
			Button.waitForAnyPress();
			
			RegulatedMotor leftMotor = new EV3LargeRegulatedMotor(MotorPort.B);
			RegulatedMopublic static void(evade)tor rightMotor = new EV3LargeRegulatedMotor(MotorPort.C);
			DifferentialPilot pilot = new DifferentialPilot(5.0,10.5, rightMotor, leftMotor);
			
			
			
			EV3UltrasonicSensor sonicSensor = new EV3UltrasonicSensor(SensorPort.S3);
			SampleProvider sp2 = sonicSensor.getDistanceMode();
			float[] sample2 = new float[sp2.sampleSize()];
			
			
			
			sp2.fetchSample(sample2, 0);
			// until the escape button is pressed
			while(Button.ESCAPE.isUp()) {
				sp2.fetchSample(sample2, 0);
				pilot.travel(-30);
				pilot.rotate(80);
				pilot.travel(-30);
				pilot.rotate(80);
				pilot.travel(-30);
				pilot.rotate(80);
				pilot.travel(-30);
				pilot.rotate(80);
				pilot.travel(-30);
				pilot.rotate(80);
				pilot.travel(-30);
				pilot.rotate(80);
				pilot.travel(-30);
				pilot.rotate(80);
				
				if(sample2[0] < .15){
				
                            
			
		
			pilot.arcBackward(25);
			pilot.rotate(90);
			pilot.travel(-75);
			pilot.arcBackward(50);
			pilot.rotate(-45);
			sonicSensor.close();
			}
		}
	}
	
}

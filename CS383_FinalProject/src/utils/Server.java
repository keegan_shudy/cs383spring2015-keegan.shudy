package utils;


import java.net.*;
import java.util.*;
import java.io.*;

import lejos.utility.Delay;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.Button;


public class Server{
	protected static String ip;
	protected static int port;
	protected static PrintWriter out;
	protected static ServerSocket srvr;
	protected static Socket skt = null;
	
	private Server(){
		throw new AssertionError();
	}
	
	/**
	 * allows the robot to open a connection on a given port number
	 * @param portNumber - the port to host the connection on
	 * @return The PrintWriter used to communicate between the server/client
	 */
	public static PrintWriter openConnection(int portNumber){
		try {
			srvr = new ServerSocket(portNumber);
			skt = srvr.accept();
			out = new PrintWriter(skt.getOutputStream());
			Delay.msDelay(3000);
			Sound.beep();
			return out;
		} catch (Exception e) {
		}
		return null;
	}
	
	
	public static void sendInformation(String info, PrintWriter outs) {
		try {
			outs.println(info);
			outs.flush();
			LCD.drawString("Sent command: " + info, 0, 5);
			return;
		} catch (Exception e) {
		}
	}
	
	public static void waitForEnter() {
		try {
			Sound.beep();
			Button.ENTER.waitForPressAndRelease();
		} catch (Exception ex) {
		}
	}
	
	
}
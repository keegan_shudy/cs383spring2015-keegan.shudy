package utils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.*;
import javax.servlet.ServletException;

import java.net.*;
import java.util.*;
import java.io.*;

import lejos.utility.Delay;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.Button;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.motor.*;


public class Client extends HttpServlet{
	
	protected static BufferedReader in;
	
	public Client(){
		
	}
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException,java.io.IOException{
		BufferedReader reader = request.getReader();
	    String str = "";
	    while ((str = reader.readLine()) != null)
	    {
	        System.out.println(str);
	    }

	    String p = request.getParameter("action");
	    //String input = request.getParameter("action"); 
	    System.out.print(p);
	
	}
	public String parseHTMLPage(String uri){
		
		try{
			URL url = new URL("http://"+uri);
			//URLConnection urlconnect = (URLConnection) url.openConnection();
			//InputStreamReader isr = new InputStreamReader(urlconnect.getInputStream());
			//in = new BufferedReader(isr);
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String hold;
			while((hold=in.readLine()) != null){
				if(hold.contains("<label id=\"currently\">")){
					int start = hold.indexOf("<label id=\"currently\">");
					int end = hold.indexOf("</label>");
					System.out.println(hold.substring(start,end));
					return hold.substring(start,end);
				}
			}
			return ("unable to get the label and id");
			
		}catch(Exception e){return ("Something went wrong");}
		
	}
}
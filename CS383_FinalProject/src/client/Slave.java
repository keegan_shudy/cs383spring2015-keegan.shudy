package client;

import utils.Client;

import server.SimpleWebServer;

import java.net.*;
import java.util.*;
import java.io.*;

import lejos.utility.Delay;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.Button;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.motor.*;


class Slave{
	private static final DifferentialPilot motors = new DifferentialPilot(5.6f,
			12.85f, Motor.A, Motor.C);
	
	
	public static void main(String [] argv){
		SimpleWebServer smp = new SimpleWebServer("141.195.226.86",80,
				                                 new File("/home/lejos/programs/www"),false);
		
		System.out.println("Press [Enter]");
		Button.ENTER.waitForPress();
		Client c = new Client();
		String action = c.parseHTMLPage("141.195.226.86");
		Delay.msDelay(10000);
		while(Button.ESCAPE.isUp()){
		switch(action.toLowerCase()) {
    	case "up":
    		motors.travel(9999999,true);
    		break;
    		
    	case "back":
    		motors.travel(-9999999,true);
    		break;
    		
    	case "left":
    		motors.rotate(9999999,true);
    		break;
    		
    	case "right":
    		motors.rotate(-9999999,true);
    		break;
   		
    	case "stop":
    		motors.stop();
    		break;
    	/*	
    	case "plus":
    		speed += speedFactor;
    		if(speed > 500)
    			speed = 500;
    		
    		motors.setTravelSpeed(speed);
    		System.out.println("Speed is now " + speed);
    		break;

    	case "minus":
    		speed -= speedFactor;
    		if(speed <= 5)
    			speed = 5;
    		
    		motors.setTravelSpeed(speed);
    		System.out.println("Speed is now " + speed);
    		break;*/

    	default: System.out.println("Unknown");
    		
    		
    	}
		}
	}
	
	
}
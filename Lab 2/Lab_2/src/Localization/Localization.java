package Localization;
import java.io.IOException;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.hardware.Sound;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.Color;


public class Localization{
	public static Port portL = LocalEV3.get().getPort(portMenuLight());
	public static final EV3ColorSensor lightSensor = new EV3ColorSensor(portL);
	public static double lineColor; //the scanned value
	public static final double ERROR = .25; //read error % 25%
	public static final DifferentialPilot pilot = new DifferentialPilot(4.0,14.0,Motor.B, Motor.C);
	public static double currentColor; //current
	public static SampleProvider color = lightSensor.getRedMode();
    public static float[] values = new float[color.sampleSize()];
    public static double lookDistance;
    public static int count = 0;
	
	public static void main (String [] robot){
		calibrateColor();
        color.fetchSample(values,0); // read a value and put it in values[0]
        LCD.clear();
        pilot.setTravelSpeed(10);
        lookDistance = .0001; //arc distance that the robot will check when the robot goes off the line
        boolean flag = false;
        
        while(Button.ESCAPE.isUp() && !flag){
        	
        	color.fetchSample(values, 0);
        	currentColor = values[0];
        	LCD.drawString("HIGH:"  + (lineColor + (lineColor*ERROR)),0,4);
        	LCD.drawString("LOW:"  + (lineColor - (lineColor*ERROR)),0,3);
        	LCD.drawString("READING:" + currentColor,0,6);
        	pilot.travel(10000000,true);
        	update(); //
        	while(!((currentColor <= (lineColor + (lineColor*ERROR))) &&  
        			(currentColor >= (lineColor - (lineColor*ERROR)))) && Button.ESCAPE.isUp()){
        		LCD.drawString("READING:" + currentColor,0,6);
        		pilot.stop();            	
        		//if(check()) break;
        		lookLeft(lookDistance);
        		if(check()) break; //is the robot back on the line?
        		lookRight(lookDistance);//so the robot centers itself
            	//if(check()) break;
        		lookRight(lookDistance);
        		if(check()) break;
        		lookLeft(lookDistance); //centers itself
        		//if(check()) break;
        		lookDistance += .0002;
        		update(); //
        		
        		if(lookDistance > .001){ 
        			flag = true;
        			break;
        		}
        		//if(count > 5){
        		//	lookRight(lookDistance);
        		//	count = 0;
        		//	break;
        			
        		//}
        	}
        	lookDistance = .0001; //we found the line, so reset the counter
        	pilot.travel(10000000,true);
        }
        try{
        	celebrate();
        }catch(InterruptedException e){}
        lightSensor.close();
	}
	
	
	
	private static String portMenuLight(){
		String ports[] = {"Port 1", "Port 2", "Port 3", "Port 4"};
    		TextMenu portMenu = new TextMenu(ports, 1, "Light port");
    		int number = portMenu.select() + 1;
    		LCD.clear();
    		return "S" + number;
	}
	
	public static double calibrateColor(){
		LCD.clear();
        LCD.drawString("Calibrate colors", 0, 0);
       
        //calibrating colors, can be effectively expanded up to 6 in one screen or higher if output formatting doesnt matter
        LCD.drawString("Color [ENTER]", 0, 2);
        lightSensor.setFloodlight(true); //turn on the light for accurate values
        waitForEnter();
        //SampleProvider color = lightSensor.getRedMode();
        //float[] values = new float[color.sampleSize()];
        color.fetchSample(values,0); // read a value and put it in values[0]
        lineColor = values[0]; //assign the current read value to the line color to follow
        Sound.beepSequenceUp();
        
        //print out the color we just read :: DEBUG
        LCD.clear();
        LCD.drawString("Color codes", 0, 0);
        LCD.drawString("Color= " + lineColor, 0, 1);
        LCD.drawString("HIGH:"  + (lineColor + (lineColor*ERROR)),0,3);
    	LCD.drawString("LOW:"  + (lineColor - (lineColor*ERROR)),0,4);
        LCD.drawString("Press [Enter]", 0, 6);
        waitForEnter();       
        
        
        assert(values[0] == lineColor); //just double check this; doubles this method as a getcolor
        return lineColor;
	}
	
    public static void waitForEnter(){
        try{
           Sound.beep();
           Button.ENTER.waitForPressAndRelease();
        }catch(Exception ex){}
   }
    
    public static void lookLeft(double distance){
    	pilot.travelArc(0.001, distance);
    	count++;
    }
    public static void lookRight(double distance){
    	pilot.travelArc(-0.001, distance);
    	count--;
    }
    
    public static double update(){
    	color.fetchSample(values, 0);
    	currentColor = values[0];
    	assert(currentColor == values[0]);
    	return currentColor;
    }
    
    public static boolean check(){
    	currentColor = update(); //repetitive --said keegan
    	LCD.drawString("READING:" + currentColor,0,6);
    	if(!((currentColor <= (lineColor + ERROR)) && 
    			currentColor >= (lineColor - ERROR)))
    		return false;
    	return true;
    }
    
    public static void celebrate() throws InterruptedException{
    	pilot.rotate(720,true);
    	for(int i = 0; i < 10; i++){
    		lightSensor.setFloodlight(Color.RED);
    		Thread.sleep(500);
    		lightSensor.setFloodlight(Color.BLUE);
    		Thread.sleep(500);
    	}
    }
}
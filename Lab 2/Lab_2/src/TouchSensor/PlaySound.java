package TouchSensor;

import java.io.IOException;
import java.io.*;

import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import java.io.IOException;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.hardware.Sound;



public class PlaySound{
	
	public static final Port portT = LocalEV3.get().getPort(portMenuTouch());
	public static final EV3TouchSensor touch = new EV3TouchSensor(portT);
	
	
	public static void main(String [] robot) throws InterruptedException{
		SampleProvider sample = touch.getTouchMode();
		float[] sample2 = new float[sample.sampleSize()];
		do{
			sample.fetchSample(sample2, 0);
			if(sample2[0] == 1){
				//int hold = Sound.playSample(new File("test.wav"), 100);
				//LCD.clear();
				//LCD.drawString("DEBUG: " + hold, 0, 1);
				
				Sound.playNote(Sound.PIANO, 4, 4);
				Sound.playNote(Sound.PIANO, 1, 3);
				Thread.sleep(500);
				Sound.playNote(Sound.PIANO, 1, 3);
				Sound.playNote(Sound.FLUTE, 2, 2);
				Thread.sleep(500);
				Sound.playNote(Sound.FLUTE, 4, 4);
				Thread.sleep(500);
				
				Sound.playNote(Sound.PIANO, 8, 4);
				Sound.playNote(Sound.PIANO, 1, 3);
				Thread.sleep(500);
				Sound.playNote(Sound.PIANO, 10, 3);
				Sound.playNote(Sound.FLUTE, 2, 2);
				Thread.sleep(500);
				Sound.playNote(Sound.FLUTE, 8, 4);
				Sound.playNote(Sound.FLUTE, 4, 4);
				Sound.playNote(Sound.FLUTE, 10, 4);
				Thread.sleep(500);
				
				Sound.playNote(Sound.PIANO, 12, 4);
				Thread.sleep(300);
				Sound.playNote(Sound.PIANO, 1, 3);
				
				Sound.playNote(Sound.PIANO, 1, 3);
				Sound.playNote(Sound.FLUTE, 100, 2);
				Thread.sleep(100);
				Sound.playNote(Sound.FLUTE, 4, 4);
				Sound.playNote(Sound.FLUTE, 16, 4);
				Sound.playNote(Sound.FLUTE, 16, 4);
				Thread.sleep(2000);
			}
		}while(Button.ESCAPE.isUp());
		
	}
	
	
	private static String portMenuTouch(){
		String ports[] = {"Port 1", "Port 2", "Port 3", "Port 4"};
    		TextMenu portMenu = new TextMenu(ports, 1, "Touch port");
    		int number = portMenu.select() + 1 ;
    		LCD.clear();
    		return "S" + number;
	}
}
package ColorSensor;

import java.io.IOException;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.hardware.Sound;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.DifferentialPilot;

public class FollowLine{
	public static Port portL = LocalEV3.get().getPort(portMenuLight());
	public static final EV3ColorSensor lightSensor = new EV3ColorSensor(portL);
	public static double lineColor;
	public static final double ERROR = .15;
	
	
	
	public static void main(String[] robot){
		calibrateColor();
		DifferentialPilot pilot = new DifferentialPilot(4.0,14.0,Motor.B, Motor.C);
		SampleProvider color = lightSensor.getRedMode();
        float[] values = new float[color.sampleSize()];
        color.fetchSample(values,0); // read a value and put it in values[0]

        LCD.clear();

        while((values[0] <= (lineColor + ERROR)) 
        		&& (values[0] >= (lineColor - ERROR)) && Button.ESCAPE.isUp()){
        color.fetchSample(values,0);
        
        pilot.forward();
        LCD.drawString("COLOR = " + values[0], 0,1);
        LCD.drawString("COLOR HIGH = " + (lineColor + ERROR), 0, 3);
        LCD.drawString("COLOR LOW = " + (lineColor - ERROR), 0, 5);
        }
        pilot.stop();
        waitForEnter();
        LCD.clear();
        LCD.drawString("Finished" ,0,4);
        lightSensor.setFloodlight(false);
        lightSensor.close();
	}


	private static String portMenuLight(){
		String ports[] = {"Port 1", "Port 2", "Port 3", "Port 4"};
    		TextMenu portMenu = new TextMenu(ports, 1, "Light port");
    		int number = portMenu.select() + 1;
    		LCD.clear();
    		return "S" + number;
	}
	
	public static double calibrateColor(){
		LCD.clear();
        LCD.drawString("Calibrate colors", 0, 0);
       
        //calibrating colors, can be effectively expanded up to 6 in one screen or higher if output formatting doesnt matter
        LCD.drawString("Color [ENTER]", 0, 2);
        lightSensor.setFloodlight(true); //turn on the light for accurate values
        waitForEnter();
        SampleProvider color = lightSensor.getRedMode();
        float[] values = new float[color.sampleSize()];
        color.fetchSample(values,0); // read a value and put it in values[0]
        lineColor = values[0]; //assign the current read value to the line color to follow
        Sound.beepSequenceUp();
        
        //print out the color we just read :: DEBUG
        LCD.clear();
        LCD.drawString("Color codes", 0, 0);
        LCD.drawString("Color= " + lineColor, 0, 1);
        LCD.drawString("Press [Enter]", 0, 4);
        waitForEnter();       
        
        
        assert(values[0] == lineColor); //just double check this; doubles this method as a getcolor
        return lineColor;
	}
	
    public static void waitForEnter(){
        try{
           Sound.beep();
           Button.ENTER.waitForPressAndRelease();
        }catch(Exception ex){}
   }
}
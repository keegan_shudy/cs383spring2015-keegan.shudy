package UltraSonicSensor;

import java.io.IOException;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.DifferentialPilot;




public class ultraSensor {
		public static void main(String[] args) throws IOException {
			EV3UltrasonicSensor sonicSensor = new EV3UltrasonicSensor(SensorPort.S4);
			SampleProvider sp2 = sonicSensor.getMode("Distance");
			float[] sample2 = new float[sp2.sampleSize()];
			sp2.fetchSample(sample2, 0);
			
		    
			DifferentialPilot pilot = new DifferentialPilot(4.0,14.0,Motor.B, Motor.C);
			
		    
			// until the escape button is pressed
			while(Button.ESCAPE.isUp()) {
				pilot.setTravelSpeed(20.0);
				pilot.forward();
				sp2.fetchSample(sample2, 0);
				LCD.drawString("Sonic: " + sample2[0],0,4);
				while((sample2[0] > .2) && (Button.ESCAPE.isUp())){
					sp2.fetchSample(sample2, 0);
					LCD.clear();
					LCD.drawString("Sonic: " + sample2[0],0,4);
				}
				pilot.stop();
				if (Button.ESCAPE.isUp()){
				Delay.msDelay((long) (1000));
				
				//rotate
				//pilot.setRotateSpeed(120);
				pilot.rotate(130);
				}

			}
			
		}
}

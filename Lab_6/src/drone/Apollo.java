package drone;

import java.net.*;
import java.util.*;
import java.io.*;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.BrickInfo;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.remote.ev3.RMIRegulatedMotor;
import lejos.remote.ev3.RMISampleProvider;
import java.rmi.RemoteException;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.remote.ev3.RemoteEV3;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Navigator;
import lejos.hardware.motor.*;
import lejos.utility.TextMenu;

class Apollo {

	public static BufferedReader in;
	private static final DifferentialPilot pilot = new DifferentialPilot(5.6f,
			12.85f, Motor.A, Motor.C);
	private static Socket skt;

	public static void main(String[] robot) throws IOException {
		LCD.drawString("Press [Enter] to start searching",0,0);
		Button.waitForAnyPress();
		int robotNum = robotNumber();
		connect("141.195.226.8" + robotNum, 1235);
		LCD.drawString("Connection Successful",0,1);
		LCD.drawString("Press [Enter]",0,2);
		Button.waitForAnyPress();
		LCD.clear();
		// waitForData();
		while (!(getPosition("141.195.226.8" + robotNum,1235).getPose().getX() >= 999)) {
			// waitForData();
			Waypoint whereToGo = getPosition("141.195.226.8" + robotNum,1235);
			if (whereToGo == null)
				System.exit(1);
			
			Navigator nav = new Navigator(pilot);
			LCD.drawString("GOING",0,6);
			nav.goTo(whereToGo);
			while(nav.isMoving()){}
			LCD.clear();
		}
		LCD.clear();
		LCD.drawString("Recieved Stop",0,1);
	}

	/**
	 * used to establish a connection to the server robot
	 * 
	 * @param ip
	 *            - ip address of the server robot
	 * @param port
	 *            - port that the server robot has open
	 * @return true - connection was successful false - connection was
	 *         unsuccessful
	 */
	private static void connect(String ip, int port) {
		try {
			LCD.drawString("Running",0,4);
			// Socket is an endpoint for communication between two machines
			// This robot connects to the server robot at the specified ip

			while (true) { // lol, the creativity here....
				try {
					skt = new Socket(ip, port);
					break;
				} catch (Exception e) {
				}
			}
			// BufferedReader reads from an input stream
			// InputStreamReader reads bytes and converts them to characters
			// getInputStream method allows to read the data from the socket
			in = new BufferedReader(new InputStreamReader(skt.getInputStream()));

			Sound.buzz();

			LCD.drawString("Received string: ",0,5);
			Delay.msDelay(1000);

			// if the stream is not ready to be read, don't do anything, wait
			String message;
			while ((message = in.readLine()) != null) {
				LCD.drawString(message,0,6);
			}

			// System.out.println(in.readLine()); // Read one line and output
			// Delay.msDelay(3000);
			// Sound.buzz();
			// Delay.msDelay(3000);
			// System.out.print("\n");

			// Button.waitForAnyPress();
			// in.close();

		} catch (Exception e) {
			System.out.print("Whoops! It didn't work!\n");
		}
	}

	/**
	 * holds buffered reader until there is something to read or well, thats the
	 * idea
	 */
	public static void waitForData() throws IOException {
		// in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
		try {
			while (in.readLine() == null) {
			}
		} catch (Exception e) {
		}
		in.close();
	}

	/**
	 * read the position that the server robot is at and relay the information
	 * to the client robot
	 * 
	 * @return Waypoint - the location of the server robot
	 */
	public static Waypoint getPosition(String ip, int port) {
		String message;
		Waypoint wp = null;
		LCD.drawString("attempting",0,0);

		//in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
		while (true) {
			try {
				skt = new Socket(ip, port);
				in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
				
				while ((message = in.readLine()) != null) {
					LCD.drawString(message, 0,5);
					if (message.contains(",")) {
						wp = new Waypoint(
								Double.valueOf(message.split(",")[0]),
								Double.valueOf(message.split(",")[1]));
						LCD.drawString("got a WP",0,1);
						LCD.drawString("WP:" + message,0,4);
						return wp;
					}
				}
			} catch (Exception e) {
			} // unhandled
		}
		
	}
	
	public static int robotNumber() {
		String ports[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
		TextMenu portMenu = new TextMenu(ports, 1, "Select " + " Robot");
		int number = portMenu.select() + 1;
		LCD.clear();
		return number;
	}
}
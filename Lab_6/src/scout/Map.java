package scout;

import java.util.*;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.*;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.geometry.Line;
import lejos.robotics.geometry.Point;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.DijkstraPathFinder;
import lejos.robotics.mapping.LineMap;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.lcd.LCD;

class Map {

	public static Path pathing = new Path();
	private int height, width, passes;
	private Navigator nav;

	public Path generateMap(int height, int width, int passes) {
		this.height = height;
		this.width = width;
		this.passes = passes;
		for (int i = 0; i < passes; i++) {
			int movingInterval = height / passes;
			pathing.add(new Waypoint(new Point(i*movingInterval, (i%2)*width)));//doing the vertical lines 
			pathing.add(new Waypoint(new Point(i*movingInterval, width)));//doing the horizontal lines
		}
		return pathing;

	}
	
	public boolean runPath(DifferentialPilot motors){
		if(pathing.size() == 0) return false;
		
		nav = new Navigator(motors);
		nav.clearPath();
		nav.followPath(pathing);
		//while(!nav.pathCompleted()) {}
		return true;
		
	}
	
	public Point getPoint(){
		Pose p = nav.getPoseProvider().getPose();
		return p.getLocation();
	}
	
	/*
	private LineMap getLineMap(){
		LineMap holder;
		Line[] lines = new Line[pathing.size()];
		for(int i = 0; i < lines.length-1; i++){
			lines[i] = new Line(pathing.get(i).getPose().getX(), pathing.get(i).getPose().getX(),
					pathing.get(i+1).getPose().getX(), pathing.get(i+1).getPose().getY());
		}
		
		holder = new LineMap(lines);
		return holder;
		
	}
	public Path findShortestRoute(Pose current, Waypoint ending){
		
	
	
	}*/
	
	// this main is solely for testing purposes
	/*
	public static void main(String[] ards) {
		DifferentialPilot pilot = new DifferentialPilot(5.6f,12.85f, Motor.A, Motor.C);
		Map m = new Map();
		Path follow = m.generateMap(100,100,5);
		Navigator nav = new Navigator(pilot);
		nav.clearPath();
		System.out.println(follow.size());
		//for(int i = 0; i < follow.size(); i++){
		//	System.out.println(follow.get(i).getPose().getX() + "," + follow.get(i).getPose().getY());
		//	nav.goTo(follow.get(i+1));
		//	while(nav.isMoving()){}
		//}
		nav.followPath(follow);
		LCD.drawString("Driving",0,1);
		while(!nav.pathCompleted()){}
		LCD.drawString("Complete",0,1);
	}*/
}
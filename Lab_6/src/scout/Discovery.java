package scout;

import java.io.*;
import java.util.*;
import java.net.*;
import java.text.DecimalFormat;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.hardware.Sound;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.Color;
import lejos.robotics.geometry.*;
import lejos.robotics.navigation.Pose;
import lejos.robotics.pathfinding.ShortestPathFinder;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.filter.MeanFilter;

class Discovery {
	private static List<Float> blacklist = new ArrayList<Float>();
	private static Port portL = LocalEV3.get().getPort(portMenu("Light"));
	private static final EV3ColorSensor lightSensor = new EV3ColorSensor(portL);
	private static final DifferentialPilot pilot = new DifferentialPilot(5.6f,
			12.85f, Motor.A, Motor.C);
	private static SampleProvider color = lightSensor.getRedMode();
	private static SampleProvider colorMean = new MeanFilter(color, 5);
	public static float[] values = new float[color.sampleSize()];
	private static Pose pose = new Pose();
	private static DecimalFormat df = new DecimalFormat("#.#");
	private static PrintWriter out;
	private static ServerSocket srvr;
	private static Socket skt = null;

	/*
	 * instead of making a rectangle, just do, navigator.followpath creation of
	 * a path would be done by making a path object and then doing path.add(new
	 * Point(x,y)), so construct the map by hand this map should be generated in
	 * another class/file in this package called map
	 */
	public static void main(String[] robotArgs) {
		// TODO Stuff n at
		LCD.drawString("waiting for [Enter]", 0, 0);
		Button.waitForAnyPress();
		LCD.drawString("attempting...", 0, 1);
		openConnection(1235);
		LCD.drawString("connection open [Enter]", 0, 2);
		Button.waitForAnyPress();
		// }
		Delay.msDelay(3000);
		LCD.clear();
		colorMean.fetchSample(values, 0);
		blacklist.add(round(values[0]));
		pilot.setTravelSpeed(20);
		Map map = new Map();
		map.generateMap(100, 100, 5);
		map.runPath(pilot);
		while (Button.ESCAPE.isUp()) {
			sendInformation("junkdata");
			colorMean.fetchSample(values, 0);
			LCD.drawString("::" + round(values[0]), 0, 0);
			if (!readingBlacklist()) {
				pilot.stop();
				LCD.drawString("Transmitting", 0, 3);
				sendInformation(map.getPoint().getX() + "," + map.getPoint().getY());
				blacklist.add(round(values[0]));
			}
		}
		sendInformation("999,999");
		LCD.drawString("done [Enter]", 0, 5);
		waitForEnter();
		lightSensor.close();
		try {
			srvr.close();
			skt.close();
		} catch (IOException ioe) {
		}
	}

	/**
	 * check if the robot is reading a blacklisted value
	 * 
	 * @return true if color is in the blacklist false if the color isn't in the
	 *         blacklist
	 */
	public static boolean readingBlacklist() {
		colorMean.fetchSample(values, 0);
		LCD.drawString("" + round(values[0]), 0, 2);
		if (blacklist.contains(round(values[0])))
			return true;
		return false;
	}

	public static float round(float value) {
		return (Float.valueOf(df.format(value)));

	}

	/**
	 * Method used for establishing a connection between the robots
	 * 
	 * @param IP
	 *            - the ip of the robot to connect to
	 * @param portNumber
	 *            - the port number the communicating robots will use
	 * @return true if connection was established, false otherwise
	 */
	public static boolean openConnection(int portNumber) {
		try {
			srvr = new ServerSocket(portNumber);
			//skt = srvr.accept();
			//out = new PrintWriter(skt.getOutputStream());
			Delay.msDelay(3000);
			sendInformation("testing");
			Sound.beep();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public static void sendInformation(String info) {
		//while (true) {
			try {
				skt = srvr.accept();
				out = new PrintWriter(skt.getOutputStream());
				out.println(info);
				out.flush();
				out.close();
				LCD.drawString("Sent a point", 0, 5);
				return;
			} catch (Exception e) {
			}
		//}
	}

	public static String portMenu(String Type) {
		String ports[] = { "Port 1", "Port 2", "Port 3", "Port 4" };
		TextMenu portMenu = new TextMenu(ports, 1, "Select " + Type + " Port");
		int number = portMenu.select() + 1;
		LCD.clear();
		return "S" + number;
	}

	public static void waitForEnter() {
		try {
			Sound.beep();
			Button.ENTER.waitForPressAndRelease();
		} catch (Exception ex) {
		}
	}
}
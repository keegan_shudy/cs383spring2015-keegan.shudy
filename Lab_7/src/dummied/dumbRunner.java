package dummied;

import java.util.*;
import lazarus.Map;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.robotics.filter.MeanFilter;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.*;
import lejos.hardware.Sound;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.geometry.Point;
import lejos.robotics.objectdetection.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.objectdetection.Feature;
import lejos.robotics.objectdetection.FeatureDetector;
import lejos.robotics.objectdetection.FeatureListener;
import lejos.robotics.objectdetection.RangeFeatureDetector;

public class dumbRunner {

	private static Port portL = LocalEV3.get().getPort(portMenu("Ultra"));
	private static EV3UltrasonicSensor ultraSensor = new EV3UltrasonicSensor(
			portL);
	private static final DifferentialPilot pilot = new DifferentialPilot(
			Map.wheelDiameter, Map.trackWidth, Motor.A, Motor.C);
	private static SampleProvider distances = ultraSensor.getDistanceMode();
	private static MeanFilter distance = new MeanFilter(distances,1);
	public static float[] values = new float[distance.sampleSize()];
	
	public static Navigator nav = new Navigator(pilot);
	public static Point origin;
	public static void main(String[] robot) {
		distance.fetchSample(values, 0);
		int numberOfObjects = selectNumberOfObjects();
		int rotatePosition = 0, counter = 0;
		float lastValue = 0;
		HashMap<Double, HashMap<Double, Double>> positions = new HashMap<>(); // java 7+ reliable implementation
		origin = nav.getPoseProvider().getPose().getLocation(); //get the origin that the robot is at
		
		while (counter != numberOfObjects) {
			distance.fetchSample(values, 0);
			nav.rotateTo(rotatePosition+= 20);
			LCD.drawString("" + rotatePosition,0,1);
			if (values[0] < 1.25 && 
					(((lastValue-(lastValue*.3)) <= values[0]) || (values[0] <= (lastValue+(lastValue*.3))))) {
				// adding the rank, position, and angle of detected objects to
				// the
				// hashmap
				lastValue = values[0];
				counter++;
				positions.put(Double.valueOf("" + Math.abs(1/values[0])),
						new HashMap<Double, Double>());
				positions.get(Double.valueOf("" + Math.abs(1/values[0])))
						.put(Double.valueOf(values[0]),
								Double.valueOf(rotatePosition)); // rank,distance,rotation
			}
		}
		nav.stop();
		pilot.quickStop();
		LCD.clear();
		TreeMap<Double, HashMap<Double, Double>> sorteds = new TreeMap<>(
				positions); // java 7+ reliable implementation
		SortedMap <Double, HashMap<Double, Double>> sorted = sorteds.descendingMap();
		
		HashMap<Double, Double> inner;
		Double [] keys = new Double[sorted.keySet().toArray().length];
		int count = 0;
		for(Object obj : sorted.keySet().toArray()){
			if(obj instanceof Double){
				keys[count] = (Double) obj;
				count++;
			}
		}
		for(int i=0; i < keys.length; i++)
			LCD.drawString(""+keys[i],0,i);
		Button.waitForAnyPress();
		LCD.clear();
		/*
		 * since the walls will be the outer bounds, they will also be the
		 * farthest objects away so ignore anything past the number of objects
		 * we know
		 */
		Double distance, rotation;
		for (int i = 0; i < numberOfObjects; i++) {
			nav.stop();
			pilot.quickStop();
			nav.rotateTo(0); //reset the robots heading
			Delay.msDelay(300);
			inner = sorted.get(keys[i]);
			LCD.drawString("" + inner.size(),0,6);
			distance = (Double) inner.keySet().toArray()[0];
			rotation = inner.get(distance);
			LCD.drawString("" + distance,0,0);
			LCD.drawString("" + rotation,0,1);
			Button.waitForAnyPress();
			nav.rotateTo(rotation);
			pilot.travel((distance*110));
			goToPoint(origin);
			LCD.clear(0,0,30);
			LCD.clear(0,1,30);

		}
		ultraSensor.close();
	}

	public static boolean returnToOrigin() {
		LCD.drawString("origin",4,4);
		return goToPoint(new Point(0.0f, 0.0f));

	}
	
	public static boolean goToPoint(Point location) {
		nav.clearPath();
		nav.goTo(new Waypoint(location));
		while (!nav.pathCompleted()) {
		}
		LCD.clear(4,4,6);
		return true;
	}

	public static int selectNumberOfObjects() {
		String inputs[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		TextMenu portMenu = new TextMenu(inputs, 1, "How Many Objects");
		int number = portMenu.select() + 1;
		LCD.clear();
		return number;

	}

	public static String portMenu(String Type) {
		String ports[] = { "Port 1", "Port 2", "Port 3", "Port 4" };
		TextMenu portMenu = new TextMenu(ports, 1, "Select " + Type + " Port");
		int number = portMenu.select() + 1;
		LCD.clear();
		return "S" + number;
	}
}

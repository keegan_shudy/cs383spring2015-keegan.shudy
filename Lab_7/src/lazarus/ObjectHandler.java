package lazarus;

import java.util.*;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.*;
import lejos.hardware.Sound;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.geometry.Point;
import lejos.robotics.objectdetection.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.objectdetection.Feature;
import lejos.robotics.objectdetection.FeatureDetector;
import lejos.robotics.objectdetection.FeatureListener;
import lejos.robotics.objectdetection.RangeFeatureDetector;

public class ObjectHandler {

	 private static List<Float> blacklist = new ArrayList<Float>();
	private static Port portL = LocalEV3.get().getPort(portMenu("Light"));
	private static final EV3UltrasonicSensor ultraSensor = new EV3UltrasonicSensor(
			portL);
	private static final DifferentialPilot pilot = new DifferentialPilot(
			Map.wheelDiameter, Map.trackWidth, Motor.A, Motor.C);
	private static SampleProvider distance = ultraSensor.getDistanceMode();
	private static Port portG = LocalEV3.get().getPort(portMenu("Gyro"));
	private static final EV3UltrasonicSensor gyroSensor = new EV3UltrasonicSensor(
			portG);
	private static SampleProvider orientation = ultraSensor.getDistanceMode();
	  public static float[] values = new float[distance.sampleSize()
			+ orientation.sampleSize()]; // note, this should equal 2

	/*public static void main(String[] robot) {
		Map map = new Map();
		map.generateMap(80, 80, 5);
		Navigator jarvis = map.runPath(pilot);
		Point position;
		int size = selectNumberOfObjects(), objCounter = 0;
		Point[] objects = new Point[size]; // stores where the objects are
		LCD.drawString("[Enter] to Begin", 0, 0);
		LCD.drawString("[Escape] to quit", 0, 1);
		waitForEnter();
		LCD.clear();

		while ((objects[size] != null) && Button.ESCAPE.isUp()) {
			position = objectDetector(jarvis);
			 there is nothing in our way, so do nothing 
			if (position == null)
				continue;
			note that we might need to offset the 'y' position here for orientation purposes
			objects[objCounter++] = position; // store the objects position
		}

		for (int i = 0; i < objects.length; i++)
			LCD.drawString("(" + objects[i].getX() + "," + objects[i].getY()
					+ ")", 0, i);

		 after printing out the points, return to the origin 
		returnToOrigin();
		LCD.drawString("Press [Enter]", 0, objects.length + 1);
		waitForEnter();
		LCD.clear();
		
		here we go to the location of the object
		 * then we make sure the we are orientated correctly
		 * then we move the box back to the origin
		for (int i = 0; i < objects.length; i++) {
			goToPoint(objects[i]);
			orientate();
			returnToOrigin();
		}
		ultraSensor.close();
	}*/

	/**
	 * checks to see if the robot ran into an object or will run into an object
	 * note: this method does not prevent the robot from running into the object
	 * 
	 * @param navi
	 *            - the navigator that the robot is using for following its path
	 * @return the point where the box that the robot ran into should be or null
	 *         if there was no object
	 */
	public static Point objectDetector(Navigator navi) {
		distance.fetchSample(values, 0);
		if (values[0] <= 0.3) {
			return new Point((navi.getWaypoint().getPose().getX()), (navi
					.getWaypoint().getPose().getY()));
		}
		return null;
	}

	/**
	 * 
	 * @param m
	 *            - Map that the robot is currently traversing
	 * @return the x coordinate that the robot is currently at
	 */
	public static double getX(Map m) {
		return (m.getPoint().getX());
	}

	/**
	 * 
	 * @param m
	 *            - Map that the robot is currently traversing
	 * @return the y coordinate that the robot is currently at
	 */
	public static double getY(Map m) {
		return (m.getPoint().getY());
	}

	/**
	 * returns the robot to the origin (0,0) note: this method holds until the
	 * robot gets to (0,0)
	 * 
	 * @return true - once the robot reaches its destination false - if the
	 *         robot does not reach the origin
	 */
	public static boolean returnToOrigin(Map m) {
		Path p = new Path();
		p.add(new Waypoint(new Point(0, 0)));
		m.runPath(pilot, p, true);
		return true;
	}

	public static boolean returnToOrigin() {
		return goToPoint(new Point(0, 0));

	}

	public static void orientate() {
		orientation.fetchSample(values, 1);
		pilot.setRotateSpeed(10);
		while(values[1] % 360 != 0){
			orientation.fetchSample(values,1);
			if(values[1] < 360)
				pilot.rotate(360 - values[0]);
			else
				pilot.rotate((values[1] - (values[1]%360)));
		}
	}
	//rotate to a specified angle
	public static void orientate(double angle) {
		orientation.fetchSample(values, 1);
		pilot.setRotateSpeed(10);
		while(values[1] % angle != 0){
			orientation.fetchSample(values,1);
			if(values[1] < angle)
				pilot.rotate(angle - values[0]);
			else
				pilot.rotate((values[1] - (values[1]%angle)));
		}
	}

	/**
	 * takes to robot to a specific point; note: this method holds until the
	 * robot gets to the point
	 * 
	 * @param location
	 *            - the point to travel to
	 * @return true - once the robot reaches its location
	 */
	public static boolean goToPoint(Point location) {
		Navigator nav = new Navigator(pilot);
		nav.clearPath();
		nav.goTo(new Waypoint(location));
		LCD.drawString("nav",4,4);
		while (!nav.pathCompleted()) {
		}
		return true;
	}

	public static int selectNumberOfObjects() {
		String inputs[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		TextMenu portMenu = new TextMenu(inputs, 1, "How Many Objects");
		int number = portMenu.select() + 1;
		LCD.clear();
		return number;

	}

	public static String portMenu(String Type) {
		String ports[] = { "Port 1", "Port 2", "Port 3", "Port 4" };
		TextMenu portMenu = new TextMenu(ports, 1, "Select " + Type + " Port");
		int number = portMenu.select() + 1;
		LCD.clear();
		return "S" + number;
	}

	public static void waitForEnter() {
		try {
			Sound.beep();
			Button.ENTER.waitForPressAndRelease();
		} catch (Exception ex) {
		}
	}
}
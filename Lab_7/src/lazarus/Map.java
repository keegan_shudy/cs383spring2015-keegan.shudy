package lazarus;

import java.util.*;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.*;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.geometry.Line;
import lejos.robotics.geometry.Point;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.DijkstraPathFinder;
import lejos.robotics.mapping.LineMap;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.lcd.LCD;

public class Map {

	public static Path pathing = new Path();
	private int height, width, passes;
	public static double wheelDiameter = 5.6, trackWidth =12.58;
	private Navigator nav;

	public Path generateMap(int height, int width, int passes) {
		this.height = height;
		this.width = width;
		this.passes = passes;
		for (int i = 0; i < passes; i++) {
			int movingInterval = height / passes;
			/*pathing.add(new Waypoint(new Point(i*movingInterval, width)));//doing the vertical lines 
			if(i == 3 || (i-1/*4*///) % 2/*4*/ == 0)
			/*	pathing.add(new Waypoint(new Point(i*movingInterval, 0)));//doing the horizontal lines
			else
				pathing.add(new Waypoint(new Point((i+1)*movingInterval, width)));//doing the down vertical
			*/
			
			/*note that the order of this is important
			 * also, this is not the same as the following:
			 * 
			 * point(i*mI,0)
			 * point(i*mI, width)
			 * point((i+1)*mI,width)
			 */
			if(i % 2 == 0)
				pathing.add(new Waypoint(new Point((i)*movingInterval, 0))); //adding the bottom left corner
			
			pathing.add(new Waypoint(new Point((i)*movingInterval, width))); //adding the top left corner
			
			if( (i == 3) || ( (i-1) % 2 == 0) )
				pathing.add(new Waypoint(new Point((i)*movingInterval, 0))); //adding the top right corner
			//else
			//	pathing.add(new Waypoint(new Point((i+1)*movingInterval, width))); //adding the top right corner
		
		}
		return pathing;

	}
	
	public Navigator runPath(DifferentialPilot motors){
		if(pathing.size() == 0) throw new RuntimeException("Path has 0 nodes \n did you generate the map?");
		
		nav = new Navigator(motors);
		nav.clearPath();
		nav.followPath(pathing);
		return nav;
		
	}
	
	public boolean runPath(DifferentialPilot motors, Path path, boolean waitTillFinished){
		if(pathing.size() == 0) return false;
		
		nav = new Navigator(motors);
		nav.clearPath();
		nav.followPath(path);
		if(waitTillFinished) while(!nav.pathCompleted()) {}
		return true;
		
	}
	
	public Point getPoint(){
		Pose p = nav.getPoseProvider().getPose();
		return p.getLocation();
	}
	
	/*
	private LineMap getLineMap(){
		LineMap holder;
		Line[] lines = new Line[pathing.size()];
		for(int i = 0; i < lines.length-1; i++){
			lines[i] = new Line(pathing.get(i).getPose().getX(), pathing.get(i).getPose().getX(),
					pathing.get(i+1).getPose().getX(), pathing.get(i+1).getPose().getY());
		}
		
		holder = new LineMap(lines);
		return holder;
		
	}
	public Path findShortestRoute(Pose current, Waypoint ending){
		
	
	
	}*/
	
	// this main is solely for testing purposes
	/*public static void main(String[] ards) {
		DifferentialPilot pilot = new DifferentialPilot(5.6,12.58, Motor.A, Motor.C); //5.6,12.85 4.0f,10.35f
		Map m = new Map();
		Path follow = m.generateMap(80,80,5);
		Navigator nav = new Navigator(pilot);
		nav.clearPath();
		System.out.println(follow.size());
		for(int i = 0; i < follow.size(); i++){
			System.out.println(follow.get(i).getPose().getX() + "," + follow.get(i).getPose().getY());
			nav.goTo(follow.get(i+1));
			while(nav.isMoving()){}
		}
		nav.singleStep(true);
		nav.followPath(follow);
		
		LCD.drawString("Driving",0,1);
		while(!nav.pathCompleted()){}
		LCD.drawString("Complete",0,1);
	}*/
}
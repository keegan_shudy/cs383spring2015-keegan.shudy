/**
*
* Keegan Shudy
* The point of this class is to create an agent that is a friend of the other agents
* its attendance at the bar is based upon if the agents friend(s) is/are going 
* 
**/

package elfarol;

import static elfarol.ParameterWrapper.*;

import java.util.ArrayList;
import java.util.List;

import elfarol.strategies.AStrategy;
import elfarol.strategies.RandomStrategy;


public class FriendAgent extends Agent{
	
	private boolean attend = false; //tracking if the agent is to attend that bar or not
	private List<Agent> friends = new ArrayList<Agent>(); //list of friends that the FriendAgent has
	private int numberOfFriends = 30; //default number of friends
	
	public FriendAgent(){
		assignFriends(numberOfFriends);
	}
	
	public FriendAgent(int numberOfFriends){
		this.numberOfFriends = numberOfFriends;
	
	}
	
	
	/**
	 * Returns the value of <code>attend</code>.
	 * 
	 * @return <code>true</code> if the agent attends the bar in the current
	 *         time step (<i>if</i> called after the {@link #updateAttendance()}
	 *         function); <code>false</code> otherwise
	 */
	@Override
	public boolean isAttending() {
		return attend;
	}
	
	
	
	
	/**
	 * 
	 * @param friend - the number of friends a FriendAgent is allowed to have
	 * 
	 * assigns the set number of Agent friends to the FriendAgent
	 */
	private void assignFriends(int friend){
		for(int i = 0; i < friend; i++){
			friends.add(new Agent());
		}
	}
	
	/**
	 * get whether or not the FriendAgent is attending the bar
	 */
	@Override
	public void updateAttendance() {
		final double prediction = predictAttendance(bestStrategy, History
				.getInstance().getMemoryBoundedSubHistory(), getStrategy());

		attend = (areFriendsGoing() || (prediction <= getOvercrowdingThreshold()));
		
	}
	
	/**
	 * Checks to see if more than 60% of the agent's friends are going
	 * @return
	 * 		true - if >=60% of friends are going
	 * 		false - if <60% of friends are going
	 */
	private boolean areFriendsGoing(){
		double average = 0;
		for(Agent friend : friends){
			friend.updateAttendance(); //update if the friend is going
			if(friend.isAttending()){
				++average;
			}
		}
		assert(friends.size() > 0);
		System.out.println(average + " : " + (.60 * friends.size()));
		return (average >= (.60 * friends.size()));
	}
}
/*
 * Version info:
 *     $HeadURL: https://cscs-repast-demos.googlecode.com/svn/richard/ElFarol/trunk/src/elfarol/ElFarolContextBuilder.java $
 *     $LastChangedDate: 2011-08-22 21:40:45 +0200 (H, 22 aug. 2011) $
 *     $LastChangedRevision: 1123 $
 *     $LastChangedBy: richard.legendi@gmail.com $
 */
package elfarol;

import static elfarol.ParameterWrapper.*;
import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.schedule.ScheduledMethod;

/**
 * The context builder for the <i>El Farol</i> simulation.
 * 
 * @author Richard O. Legendi (richard.legendi) | Modified by: Keegan Shudy and Scott Meltzer
 * @since 2.0-beta, 2011 | Modified 2015
 * @version $Id: ElFarolContextBuilder.java 514 2011-07-08 14:50:51Z
 *          richard.legendi@gmail.com $
 *          
 *          
 *          overall, this whole class was changed to add in the ability 
 *          to switch between the regular agent and the friend agent.
 *          
 *          To accomplish this goal the following files were changed:
 *          	Utils.java
 *          	parameterWrapper.java
 *          The following files were added:
 *          	FriendAgent.java
 *          	
 *          
 * @instructions: to run as a regular agent, open up the Parameter Wrapper and change 
 * 			'private static String Agent' to equal 'Default'
 * 				  to run as friend agent, change 'private static String Agent' to equal 'friend-agent'
 */


public class ElFarolContextBuilder extends DefaultContext<Object> implements
		ContextBuilder<Agent> {

	
	
	//private boolean isFriendAgent = getAgent(); //is the agent a friend agent?
	// ========================================================================
	// === Super Interface ====================================================

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * repast.simphony.dataLoader.ContextBuilder#build(repast.simphony.context
	 * .Context)
	 */
	@Override
	public Context<Agent> build(final Context<Agent> context) {
		if(getAgent()){
			ParameterWrapper.reinit();
			History.getInstance().init();
			System.out.println("Friend\n\n");
				for (int i = 0; i < getAgentsNumber(); ++i) {
				final FriendAgent agent = new FriendAgent();
				context.add(agent);
			}
		}
		else{
			ParameterWrapper.reinit();
			History.getInstance().init();
			System.out.println("Agent\n\n");
			for (int i = 0; i < getAgentsNumber(); ++i) {
				final Agent agent = new Agent();
				context.add(agent);
			}
			
		}
		return context;
		
	}

	// ========================================================================
	// === Schedule ===========================================================

	/**
	 * Main schedule of the simulation containing the following phases:
	 * 
	 * <ol>
	 * <li><b>First</b>, all agents determine if their attend to the bar based
	 * on their knowledge (<i>strategies</i>) and the prediction</li>
	 * <li><b>Then</b> we update the global history</li>
	 * <li><b>At the end of the turn</b> when the new attendance level is known,
	 * all of the agents update their best strategy</li>
	 * </ol>
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void schedule() {
		if(getAgent()){
		
			for (final FriendAgent act : Utils.getFriendAgentList()) {
				act.updateAttendance();
			}

			History.getInstance().updateHistory();

			for (final FriendAgent act : Utils.getFriendAgentList()) {
				act.updateBestStrategy();
			}
		}
		else{
			for (final Agent act : Utils.getAgentList()) {
				act.updateAttendance();
			}

			History.getInstance().updateHistory();

			for (final Agent act : Utils.getAgentList()) {
				act.updateBestStrategy();
			}
		}
	}
	// ========================================================================
	// === Observer Methods ===================================================

	/**
	 * Returns the current attendance level of the bar to display in a simple
	 * time series chart on the Repast GUI.
	 * 
	 * @return the current attendance level of the bar
	 */
	public int attendance() {
		if(getAgent()) return Utils.getFriendAttendance();
		
		else return Utils.getAttendance();
	}

}
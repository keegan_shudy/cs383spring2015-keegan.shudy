/*
 * Keegan Shudy
 * 
 *  Unused and will not be implemented as this is not necessary
 */
package elfarol.strategies;

import static elfarol.ParameterWrapper.getMemorySize;
import elfarol.strategies.RandomStrategy;
import elfarol.History;
import java.util.*;


public class AverageStrategy extends AStrategy{
	protected final double[] weights = {};
	private static List<Integer> weightsFromWeeks;
	
	private static double[] getAverageWeights() {
		final double[] ret = new double[getMemorySize() + 1];

		for (int i = 0; i < ret.length; ++i) {
			ret[i] = fillWeightsFrom(i);
		}

		return ret;
	}

	public AverageStrategy() {
		super(getAverageWeights());
	}
	

	private static double fillWeightsFrom(int week){
		double average;
		try{
			weightsFromWeeks = History.getInstance().getSubHistory(week);
			
		}catch(Exception e){
			System.out.println("something went wrong, defaulting to random strategy");
			new RandomStrategy();
		}finally{
			int i;
			average = 0;
			for(i = 0; i < weightsFromWeeks.size(); i++)
				average += weightsFromWeeks.get(i);
			average /= i;
		}
		return average;
	}
}
